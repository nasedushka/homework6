package com.mycompany.l08;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import java.io.File;

public class UploadDownloadTests extends TestBase {

    @Test
    public void uploadFile() {
        String url = "https://yandex.ru/images/";
        driver.get(url);
        driver.findElement(By.cssSelector(".input__cbir-button.input__button")).click();
        driver.findElement(By.cssSelector(".cbir-panel__file-input")).sendKeys("C:\\Users\\onasedki\\IdeaProjects\\lec6_okna\\src\\test\\resources\\test_automation.png");
        assert driver.findElement(By.cssSelector(".cbir-title.other-sites__head")).getText().equals("Сайты с информацией про изображение");
    }

    @Test
    public void downloadFile() throws InterruptedException {
        String url = "https://ru.vividscreen.info/";
        driver.get(url);
        driver.findElements(By.cssSelector(".list-item-link")).get(0).click();

        driver.switchTo().frame("ad_iframe");

        driver.findElements(By.cssSelector(".item-links-download a")).get(0).click();
        Thread.sleep(10000);
        isFileDownloaded("BMW-X7-Lumma-CLR-wide.jpg");
    }

    private boolean isFileDownloaded(String fileName) {
        File dir = new File("C:\\Users\\onasedki\\IdeaProjects\\lec6_okna\\src\\test\\resources");
        File[] dirContents = dir.listFiles();

        assert dirContents != null;
        for (File dirContent : dirContents) {
            if (dirContent.getName().equals(fileName)) {
                dirContent.delete();
                return true;
            }
        }
        return false;
    }

}
