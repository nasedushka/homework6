package com.mycompany.l08;

import com.mycompany.l08.capabilities.BrowserCapabilities;
import com.mycompany.l08.capabilities.Browsers;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.util.concurrent.TimeUnit;

public class TestBase {

    static final String STATIC_RESOURCES = "src/test/resources/static/";
    WebDriver driver;

    @BeforeMethod
    public void initData() {
        //var capabilities = BrowserCapabilities.getCapabilities(Browsers.FIREFOX);
        //driver = new FirefoxDriver((FirefoxOptions) capabilities);

        var capabilities = BrowserCapabilities.getCapabilities(Browsers.CHROME);
        driver = new ChromeDriver((ChromeOptions) capabilities);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @AfterMethod
    public void stop() {
        if (driver != null) {
            driver.quit();
        }
    }

}
