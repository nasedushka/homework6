package com.mycompany.l08;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import static org.openqa.selenium.By.*;
import static org.openqa.selenium.support.ui.ExpectedConditions.frameToBeAvailableAndSwitchToIt;

public class FrameTests extends TestBase {

    private String url = "https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_frame_cols";

    @Test
    public void testFrame() throws InterruptedException {
        driver.get(url);

        WebElement iframeResult = driver.findElement(id("iframeResult"));
        driver.switchTo().frame(iframeResult);

        //go to frameA
        new WebDriverWait(driver, 5)
                .until(frameToBeAvailableAndSwitchToIt(cssSelector("[src = 'frame_a.htm']")));
        var textInFrameA = driver.findElement(tagName("h3")).getText();
        System.out.println(textInFrameA);

        //go to a parent frame
        driver.switchTo().parentFrame();

        //go to the default content
        driver.switchTo().defaultContent();
        driver.findElement(id("tryhome")).click();

        Thread.sleep(2_000);
    }
}
