package com.mycompany.l08.capabilities;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.CapabilityType;

import java.io.File;
import java.util.HashMap;

public class BrowserCapabilities {

    private static String downloadDir = new File("src/test/resources").getAbsolutePath();

    public static Capabilities getCapabilities(Browsers browser) {
        MutableCapabilities capabilities;
        Proxy proxy = new Proxy()
                .setProxyAutoconfigUrl("http://wpadvrn.t-systems.ru/wpad.dat");

        switch (browser) {
            case FIREFOX: {
                capabilities = new FirefoxOptions()
                        .addPreference("app.update.auto", false)
                        .addPreference("app.update.enabled", false)
                        .addPreference("network.proxy.no_proxies_on", "localhost")

                        //preferences for file download
                        .addPreference("browser.download.dir", downloadDir)
                        .addPreference("browser.download.folderList", 2)
                        .addPreference("browser.helperApps.neverAsk.saveToDisk", "application/octet-stream, image/jpeg")
                        .addPreference("browser.download.manager.showWhenStarting", false);

                capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
                break;
            }

            case CHROME: {
                HashMap<String, Object> prefs = new HashMap<>();
                prefs.put("profile.default_content_settings.popups", 0);
                prefs.put("download.default_directory", downloadDir);

                capabilities = new ChromeOptions()
                        .addArguments("--start-maximized")

                        //preferences for file download
                        .addArguments("--ignore-certificate-errors")
                        .setExperimentalOption("prefs", prefs)
                        .addArguments("--test-type")
                        .addArguments("--disable-extensions")
                        .setProxy(proxy);
                break;
            }

            default:
                throw new IllegalArgumentException("Invalid browser: " + browser);
        }

        capabilities.setCapability(CapabilityType.SUPPORTS_JAVASCRIPT, true);
        capabilities.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.ACCEPT);
        return capabilities;
    }
}
